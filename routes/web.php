<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Frontend Route
Route::get('/','HomeController@index');
Route::get('/productByCategory/{id}','HomeController@productByCategory')->name('category.products');
Route::get('/productByManufacture/{id}','HomeController@productByManufacture')->name('manufacture.products');
Route::get('/product/details/{id}', 'HomeController@details')->name('product.details');

//Route::post('/add-to-cart','CartController@add_to_cart');
//Route::get('/show-cart','CartController@show_cart');
Route::resource('cart','CartController');
//Backend Route
Route::get('/admin','AdminController@index');
Route::get('/dashboard','SuperAdminController@index');
Route::post('/admin-dashboard','AdminController@dashboard');
Route::get('/logout','SuperAdminController@logout');

Route::resource('category','Admin\CategoryController');
Route::get('/unactive_category/{id}','Admin\CategoryController@unactive');
Route::get('/active_category/{id}','Admin\CategoryController@active');


Route::resource('manufacture','Admin\ManufactureController');
Route::get('/unactive_manufacture/{id}','Admin\ManufactureController@unactive');
Route::get('/active_manufacture/{id}','Admin\ManufactureController@active');

Route::resource('product','Admin\ProductController');
Route::get('/unactive_product/{id}','Admin\ProductController@unactive');
Route::get('/active_product/{id}','Admin\ProductController@active');

Route::resource('slider','Admin\SliderController');
Route::get('/unactive_slider/{id}','Admin\SliderController@unactive');
Route::get('/active_slider/{id}','Admin\SliderController@active');


