@extends('layouts.backend')
@section('content')
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Manufacture View</a></li>
    </ul>

    <div class="row-fluid sortable">
           <h1 style="color: #0044cc"> <b>Manufacture View</b></h1>
        <a class="btn btn-primary pull-right" href="{{route('manufacture.index')}}">
            Manufacture
        </a>
        <br/>
        <br/>

       <p> <b>manufacture Name:</b>  {{ $manufacture->name }}</p>
       <p> <b>manufacture Description:</b>  {{ $manufacture->description }}</p>
       <p> <b>manufacture Status:</b>  {{ $manufacture->status}}</p>
    </div><!--/row-->


@endsection
