
@extends('layouts.backend')
@section('content')
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="#">Add Product</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Product</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <fieldset>

                        <div class="control-group">
                            <label class="control-label" for="name">Product Name</label>
                            <div class="controls">
                                <input type="text"  id="name" name="name">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="category">Product Category</label>
                            <div class="controls">
                                <select name="categories[]" id="category" data-rel="chosen">
                                    <option>Select Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="category">Product Manufacture</label>
                            <div class="controls">
                                <select name="manufactures[]" id="manufacture" data-rel="chosen">
                                    <option>Select Manufacture</option>
                                    @foreach($manufactures as $manufacture)
                                        <option value="{{ $manufacture->id }}">{{ $manufacture->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="control-group hidden-phone">
                            <label class="control-label" for="short_description">Product Short Description</label>
                            <div class="controls">
                                <textarea id="short_description" rows="3" name="short_description"></textarea>
                            </div>
                        </div>

                        <div class="control-group hidden-phone">
                            <label class="control-label" for="long_description">Product Long Description</label>
                            <div class="controls">
                                <textarea id="long_description" rows="3" name="long_description"></textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="price">Product Price</label>
                            <div class="controls">
                                <input type="text"  id="price" name="price">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="image">Product Image</label>
                            <div class="controls">
                                <input type="file"  id="image" name="image">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="size">Product Size</label>
                            <div class="controls">
                                <input type="text"  id="size" name="size">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="color">Product Color</label>
                            <div class="controls">
                                <input type="text"  id="color" name="color">
                            </div>
                        </div>

                        <div class="control-group hidden-phone">
                            <label class="control-label" for="status">Publication Status</label>
                            <div class="controls">
                                <input type="checkbox" name="status"  value="1">
                            </div>
                        </div>

                       <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Add product</button>
                            <a class="btn btn-primary pull-left" href="{{route('product.create')}}">
                              Reset
                            </a>
                        </div>
                    </fieldset>
                </form>

            </div>
        </div><!--/span-->

    </div><!--/row-->


@endsection