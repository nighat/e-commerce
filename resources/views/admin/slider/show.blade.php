@extends('layouts.backend')
@section('content')
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Category View</a></li>
    </ul>

    <div class="row-fluid sortable">
           <h1 style="color: #0044cc"> <b>Category View</b></h1>
        <br/>
        <br/>

       <p> <b>Category Name:</b>  {{ $category->name }}</p>
       <p> <b>Category Description:</b>  {{ $category->description }}</p>
       <p> <b>Category Status:</b>  {{ $category->status}}</p>
    </div><!--/row-->


@endsection
