@extends('layouts.backend')
@section('content')
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Slider Table</a></li>
    </ul>

    <div class="row-fluid sortable">
        <a class="btn btn-info pull-right" href="{{route('slider.create')}}">
            Add Slider
        </a>
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Slider</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                    <tr>
                        <th>SL</th>

                        <th>Slider Image</th>
                        <th>status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>




                        @foreach($sliders as $slider)
                    <tr>
                        <td>{{ $loop->index + 1}}</td>
                        <td><img style="height:100px; width: 100px" src="{{asset('uploads/sliders/'.$slider->image)}}"></td>


                        <td class="center">
                            @if($slider->status==1)
                                <span class="label label-success">Active</span>

                            @else
                                <span class="label label-warning">Unactive</span>

                            @endif
                        </td>

                        <td class="center">
                            @if($slider->status==1)
                            <a class="btn btn-success" href="{{ URL::to('/unactive_slider/'.$slider->id) }}">
                                <i class="halflings-icon white thumbs-up"></i>
                            </a>
                            @else
                                <a class="btn btn-warning" href="{{ URL::to('/active_slider/'.$slider->id) }}">
                                    <i class="halflings-icon white thumbs-down"></i>
                                </a>

                            @endif
                            <a class="btn btn-info" href="{{route('slider.edit',$slider->id)}}">
                                <i class="halflings-icon white edit"></i>
                            </a>

                            </a>
                                <button class="btn btn-danger waves-effect" type="button" onclick="deleteslider({{ $slider->id }})">
                                    <i class="halflings-icon white trash"></i>
                                </button>
                                <form id="delete-form-{{ $slider->id }}" action="{{ route('slider.destroy',$slider->id) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><!--/span-->

    </div><!--/row-->


@endsection
@push('js')
<script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>
<script type="text/javascript">
    function deleteslider(id) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
            event.preventDefault();
            document.getElementById('delete-form-'+id).submit();
        }else if (
                // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
        ) {
            swal(
                    'Cancelled',
                    'Your data is safe :)',
                    'error'
            )
        }
    })
    }
</script>
@endpush