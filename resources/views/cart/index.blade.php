@extends('layouts.frontend1')
@section('content')

    <section id="cart_items">
        <div class="container col-sm-12">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Shopping Cart</li>
                    {{--<li >{{ Cart::content()->count() }}</li>--}}
                </ol>
            </div>
            <div class="table-responsive cart_info">

                <?php
                $contents= Cart::content();

                ?>
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Image</td>
                        <td class="description">Name</td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($contents as $content){

                    ?>

                    <tr>
                        <td class="cart_product">
                            {{--<a href=""><img src="{{URL::to($content->options->image) }}" alt=""></a>--}}
                            <a href=""><img src="{{asset('uploads/images/'.$content->options->image)}}" height="80px" width="80px" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{{ $content->name }}</a></h4>
                            {{--<p>{{ $content->id }}</p>--}}
                        </td>
                        <td class="cart_price">
                            <p>{{ $content->price }}</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <form action="{{ route('cart.update',$content->rowId) }}" method="post">
                                 {{ csrf_field() }}
                                 {{ method_field('PATCH') }}
                                <input class="cart_quantity_input" type="text" name="qty" value="{{ $content->qty }}" autocomplete="off" size="2">
                                <input  type="hidden" name="rowId" value="{{ $content->rowId }}">
                                <input  type="submit" name="submit" value="Update" class="btn btn-sm btn-success">

                                </form>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">{{ $content->total }}</p>
                        </td>
                        <td class="cart_delete">
                            <form id="delete-form-{{ $content->rowId }}" method="POST" action="{{ route('cart.destroy',$content->rowId) }}" style="display: none">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                            <a href="" onclick="
                                    if(confirm('Are you sure, You went to delete this?'))

                                    {
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $content->rowId }}').submit();
                                    }
                                    else{
                                    event.preventDefault();
                                    }
                                    " class=" cart_quantity_delete"><i class="fa fa-times"></i></a>

                        </td>
                            {{--<a class="cart_quantity_delete" href="{{ route('cart.destroy'.$content->rowId) }}"><i class="fa fa-times"></i></a>--}}
                        </td>
                    </tr>
                    <?php
                    }

                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </section> <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>What would you like to do next?</h3>
                <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
            </div>
            <div class="row">
                {{--<div class="col-sm-6">--}}
                {{--<div class="chose_area">--}}
                {{--<ul class="user_option">--}}
                {{--<li>--}}
                {{--<input type="checkbox">--}}
                {{--<label>Use Coupon Code</label>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<input type="checkbox">--}}
                {{--<label>Use Gift Voucher</label>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<input type="checkbox">--}}
                {{--<label>Estimate Shipping & Taxes</label>--}}
                {{--</li>--}}
                {{--</ul>--}}
                {{--<ul class="user_info">--}}
                {{--<li class="single_field">--}}
                {{--<label>Country:</label>--}}
                {{--<select>--}}
                {{--<option>United States</option>--}}
                {{--<option>Bangladesh</option>--}}
                {{--<option>UK</option>--}}
                {{--<option>India</option>--}}
                {{--<option>Pakistan</option>--}}
                {{--<option>Ucrane</option>--}}
                {{--<option>Canada</option>--}}
                {{--<option>Dubai</option>--}}
                {{--</select>--}}

                {{--</li>--}}
                {{--<li class="single_field">--}}
                {{--<label>Region / State:</label>--}}
                {{--<select>--}}
                {{--<option>Select</option>--}}
                {{--<option>Dhaka</option>--}}
                {{--<option>London</option>--}}
                {{--<option>Dillih</option>--}}
                {{--<option>Lahore</option>--}}
                {{--<option>Alaska</option>--}}
                {{--<option>Canada</option>--}}
                {{--<option>Dubai</option>--}}
                {{--</select>--}}

                {{--</li>--}}
                {{--<li class="single_field zip-field">--}}
                {{--<label>Zip Code:</label>--}}
                {{--<input type="text">--}}
                {{--</li>--}}
                {{--</ul>--}}
                {{--<a class="btn btn-default update" href="">Get Quotes</a>--}}
                {{--<a class="btn btn-default check_out" href="">Continue</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="col-sm-8">
                    <div class="total_area">
                        <ul>
                            <li>Cart Sub Total <span>{{ Cart::subtotal() }}</span></li>
                            <li>Eco Tax <span>{{ Cart::tax() }}</span></li>

                            <li>Shipping Cost <span>Free</span></li>
                            <li>Total <span>{{ Cart::total() }}</span></li>
                        </ul>
                        <a class="btn btn-default update" href="">Update</a>
                        <a class="btn btn-default check_out" href="">Check Out</a>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#do_action-->

@endsection