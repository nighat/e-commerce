<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufactureProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacture_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('manufacture_id');
           // $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            //$table->foreign('manufacture_id')->references('id')->on('manufactures')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacture_product');
    }
}
