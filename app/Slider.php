<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['status','image'];

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }
}
