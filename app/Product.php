<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description','status','image'];

    public function categories(){

        return $this->belongsToMany('App\Category')->withTimestamps();
    }

    public function manufactures(){

        return $this->belongsToMany('App\Manufacture')->withTimestamps();
    }

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }
}
