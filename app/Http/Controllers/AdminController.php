<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
session_start();
class AdminController extends Controller
{
  public  function index(){

      return view('admin_login');
  }

//    public  function show_dashboard(){
//
//        return view('admin.dashboard');
//    }
    public  function dashboard(Request $request){

       $email=$request->email;
       $password=md5($request->password);
        $result=DB::table('admins')
            ->where('email',$email)
            ->where('password',$password)
            ->first();


        if($result){
           Session::put('name',$result->name);
           Session::put('id',$result->id);
            return Redirect::to('/dashboard');
        }else{

            Session::put('message','Email or Password Invalide');
            return Redirect::to('/admin');

        }
    }


}
