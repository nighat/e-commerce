<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use Cart;
use Slider;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_published_category=DB::table('categories')
            ->where('status',1)
            ->get();
//        $manage_category=view('pages.add_cart')
//            ->with('all_published_category',$all_published_category);
          $manage_category=view('cart.index')
            ->with('all_published_category',$all_published_category);

       // return view('layouts.frontend1')->with('pages.add_cart', $manage_category);
        return view('cart.index',compact('manage_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cart.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $qty = $request->qty;
        $id=$request->id;
        $product_info=DB::table('products')
            ->where('id',$id)->first();

        $data['qty']=$qty;
        $data['id']=$product_info->id;
        $data['name']=$product_info->name;
        $data['price']=$product_info->price;
        $data['options']['image']=$product_info->image;

        Cart::add($data);
        return redirect(route('cart.index'));
   //     return Redirect::to('/show-cart');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $qty=$request->qty;
        $rowId=$request->rowId;
        Cart::update($rowId,$qty);
        return redirect(route('cart.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rowId)
    {
        Cart::update($rowId,0);
        return redirect()->back();
    }
}
