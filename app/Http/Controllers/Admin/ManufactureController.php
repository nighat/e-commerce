<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Manufacture;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Redirect;
use DB;

class ManufactureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $manufactures= Manufacture::latest()->get();
        return view('admin.manufacture.index',compact('manufactures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.manufacture.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $manufacture = new Manufacture;
        $manufacture->name = $request->name;
        $manufacture->description = $request->description;
        $manufacture->save();
        Toastr::success('Manufacture Successfully Create :)','Success');
        return redirect(route('manufacture.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Manufacture $manufacture)
    {
        return view('admin.manufacture.show',compact('manufacture'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manufacture = Manufacture::find($id);
        return view('admin.manufacture.edit',compact('manufacture'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $manufacture = Manufacture::find($id);
        $manufacture->name = $request->name;
        $manufacture->description = $request->description;
        $manufacture->save();
        Toastr::success('Manufacture Successfully Updated :)' ,'Success');
        return redirect()->route('manufacture.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Manufacture::find($id)->delete();
        Toastr::success('Manufacture Successfully Deleted :)','Success');
        return redirect()->back();
    }

    public function unactive($id)
    {
        DB::table('manufactures')->where('id',$id)
            ->update(['status' => 0]);
        Toastr::success('Manufacture Unactive Successfully  :)','Success');
        return Redirect(route('manufacture.index'));
    }

    public function active($id)
    {
        DB::table('manufactures')
            ->where('id',$id)
            ->update(['status' => 1]);
        Toastr::success('Manufacture Active Successfully  :)','Success');
        return Redirect(route('manufacture.index'));
    }



}
