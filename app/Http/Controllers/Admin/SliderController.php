<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slider;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;
use DB;
class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::latest()->get();
        return view('admin.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $slider = new Slider;
        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/sliders');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $slider->image = $fileName;
        }else{
            $slider->image = NULL;
        }
        $slider->status = $request->status;
        $slider->save();

        Toastr::success('Slider Successfully Create :)','Success');
        return redirect(route('slider.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        return view('admin.slider.show',compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::find($id);
        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/sliders');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $slider->image = $fileName;
        }else{
            $slider->image = NULL;
        }
        $slider->status = $request->status;
        $slider->save();
        Toastr::success('Slider Successfully Updated :)' ,'Success');
        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::find($id)->delete();
        Toastr::success('Slider Successfully Deleted :)','Success');
        return redirect()->back();
    }

    public function unactive($id)
    {
        DB::table('sliders')->where('id',$id)
            ->update(['status' => 0]);
        Toastr::success('Slider Unactive Successfully  :)','Success');
        return Redirect(route('slider.index'));
    }

    public function active($id)
    {
        DB::table('sliders')
            ->where('id',$id)
            ->update(['status' => 1]);
        Toastr::success('Slider Active Successfully  :)','Success');
        return Redirect(route('slider.index'));
    }




}
