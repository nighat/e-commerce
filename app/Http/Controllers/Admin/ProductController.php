<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Manufacture;
use Brian2694\Toastr\Facades\Toastr;

use Carbon\Carbon;
use DB;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products= Product::latest()->get();
        return view('admin.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories= Category::all();
        $manufactures= Manufacture::all();
        return view('admin.product.create',compact('categories','manufactures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $product = new Product;
        $product->name = $request->name;
        $product->short_description = $request->short_description;
        $product->long_description = $request->long_description;
        $product->price = $request->price;

        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/images');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $product->image = $fileName;
        }else{
            $product->image = NULL;
        }
        $product->size = $request->size;
        $product->color= $request->color;
        $product->status= $request->status;
        $product->save();

        $product->categories()->attach($request->categories);
        $product->manufactures()->attach($request->manufactures);

        Toastr::success('product Successfully Create :)','Success');
        return redirect(route('product.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $categories= Category::all();
        $manufactures= Manufacture::all();
        $product = Product::find($id);
        return view('admin.product.edit',compact('product','categories','manufactures'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if ($request->hasFile('image')) {
            $uploadPath = public_path('/uploads/images');
            $extension = $request->image->getClientOriginalExtension();
            $timestamp = str_replace([' ', ':'], '--', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
            $fileName = $timestamp . '.' . $extension;
            $request->image->move($uploadPath, $fileName);
            $product->image = $fileName;
        }else{
            $product->image = NULL;
        }
        $product->name = $request->name;
        $product->short_description = $request->short_description;
        $product->long_description = $request->long_description;
        $product->price = $request->price;

        $product->size = $request->size;
        $product->color= $request->color;
        $product->status = $request->status;
        $product->save();

        $product->categories()->sync($request->categories);
        $product->manufactures()->sync($request->manufactures);

        Toastr::success('Product Successfully Updated :)' ,'Success');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        Product::find($id)->delete();
        Toastr::success('product Successfully Deleted :)','Success');
        return redirect()->back();
    }

    public function unactive($id)
    {
        DB::table('products')->where('id',$id)
            ->update(['status' => 0]);
        Toastr::success('Product Unactive Successfully  :)','Success');
        return Redirect(route('product.index'));
    }

    public function active($id)
    {
        DB::table('products')
            ->where('id',$id)
            ->update(['status' => 1]);
        Toastr::success('Product Active Successfully  :)','Success');
        return Redirect(route('product.index'));
    }




}
