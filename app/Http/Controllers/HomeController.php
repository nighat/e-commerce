<?php

namespace App\Http\Controllers;

use App\Slider;
use App\Product;
use App\Category;
use App\Manufacture;
use Cart;
use DB;
class HomeController extends Controller
{
    public  function index()
    {
        $products= Product::latest()->published()->get();
        $sliders= Slider::latest()->published()->get();
        return view('pages.home_content',compact('products','sliders'));

    }

    public function details($id)
    {
        //$products= Product::latest()->published()->get();
        $product = Product::where('id',$id)->published()->first();
        $sliders= Slider::latest()->published()->get();
        $products = Product::published()->take(3)->inRandomOrder()->get();
        return view('pages.product_detail',compact('product','sliders','products'));
    }
    public function productByCategory($id)
    {
        $category = Category::where('id',$id)->first();
        $products = $category->products()->published()->get();
        $sliders= Slider::latest()->published()->get();
        return view('pages.product_by_category',compact('products','categories','sliders'));
    }

    public function productByManufacture($id)
    {
        //$category = Category::where('id',$id)->first();
        $manufacture = Manufacture::where('id',$id)->first();
        $products = $manufacture->products()->published()->get();
        $sliders= Slider::latest()->published()->get();
        return view('pages.product_by_manufacture',compact('products','manufactures','sliders'));
    }
}
