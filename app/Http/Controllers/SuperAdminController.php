<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
session_start();

class SuperAdminController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//
//    }

    public  function index()
    {

    // $this->AdminAuthCheck();
        return view('admin.dashboard');
    }


    public function logout(){
        Session::flush();
        return Redirect::to('/admin');
    }
    public function AdminAuthCheck(){

        $admin_id = Session::get('admin_id');
        if($admin_id){
            return;;
        }else{
            return Redirect::to('/dashboard')->send();

        }
    }


}
